var express = require('express');
var router = express.Router();
const { Tuote } = require('../models');

// For image upload
var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.originalname + '-' + uniqueSuffix)
    }
});
var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        let acceptedMimeTypes = ["image/png", "image/jpg", "image/jpeg", "image/webp"];
        if (acceptedMimeTypes.includes(file.mimetype)) {
          cb(null, true);
        } else {
          cb(null, false);
          //return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});

router.get('/', (req, res, next) => {
    res.redirect(req.baseUrl+'/kaikki');
});

router.get('/kaikki', async (req, res, next) => {
    let webuser = req.session.user ? req.session.user : null;
    let tuotteet = await Tuote.findAll();
    
    res.render('kaikkiTuotteet', { tuotteet: tuotteet, webuser: webuser });
});

router.get('/info/:id', async (req, res, next) => {
    let webuser = req.session.user ? req.session.user : null;
    let tuote = await Tuote.findByPk(req.params.id);

    res.render('yksityinenTuote', { tuote: tuote, webuser: webuser });
});

router.get('/uusi', async (req, res, next) => {
    let webuser = req.session.user ? req.session.user : null;

    if (webuser && webuser.isAdmin) {
        res.render('lisätäTuote', { valmis: false, virhe: false, webuser: webuser });
    } else {
        res.redirect('/');
    }
    
});

router.post('/uusi', upload.single('kuva'), async (req, res, next) => {
    let webuser = req.session.user ? req.session.user : null;
    
    if (webuser && webuser.isAdmin) {
        // TODO: Tarkistetaan lomakkeen tietoa.
        Tuote.create({
            nimi: req.body.nimi,
            kuvaus: req.body.kuvaus,
            hinta: req.body.hinta,
            kuva: '/uploads/' + req.file.filename,
            määrä: req.body.määrä
        });
        
        res.render('lisätäTuote', { valmis: true, virhe: false, webuser: webuser });
    } else {
        res.redirect('/');
    }

});


module.exports = router;