const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'database.sqlite'
});

const Tuote = sequelize.define('Tuote', {
    nimi:   { type: DataTypes.STRING, allowNull: false },
    kuvaus: { type: DataTypes.STRING                   },
    hinta:  { type: DataTypes.DECIMAL(10,2), allowNull: false },
    määrä:  { type: DataTypes.INTEGER, allowNull: false, defaultValue: 0 },
    kuva:   { type: DataTypes.STRING }
}, {
    tableName: 'Tuotteet'
});

const Käyttäjä = sequelize.define('Kayttaja', {
  käyttäjätunnus : { type: DataTypes.STRING(50),  allowNull: false, unique: true        },
  etunimi        : { type: DataTypes.STRING(50)                                         },
  sukunimi       : { type: DataTypes.STRING(50)                                         },
  sähköposti     : { type: DataTypes.STRING(50),  allowNull: false, unique: true        },
  salasana       : { type: DataTypes.STRING(100), allowNull: false                      },
  isAdmin        : { type: DataTypes.BOOLEAN,     allowNull: false, defaultValue: false }
},{
  tableName: 'kayttajat'
});

(async () => {
  // await Tuote.sync({ alter: true });
  await sequelize.sync({ alter: true });
})();

module.exports = { Tuote, Käyttäjä }