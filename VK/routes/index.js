var express = require('express');
var router = express.Router();
const { Käyttäjä, Tuote } = require('../models');

/* GET Etusivu */
router.get('/', async function(req, res, next) {
  var tuote;
  let webuser = req.session.user ? req.session.user : null;
  // if (webuser) {
  //   käyttäjä = await Käyttäjä.findByPk(webuser.id);
  //   console.log(käyttäjä.etunimi);
  // }
  let tuotteet = await Tuote.findAll();
  if (tuotteet.length > 0) {
    let tuoteIndex = Math.floor(Math.random() * tuotteet.length);
    tuote = tuotteet[tuoteIndex];
  } else {
    tuote = null;
  }
  res.render('etusivu', { esitystuote: tuote, webuser: webuser });
});

/* GET Yhteystiedot */
router.get('/yhteystiedot', (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;
  res.render('yhteystiedot', { webuser: webuser });
});

/* GET Tietosuojaseloste */
router.get('/tietosuojaseloste', (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;
  res.render('tietosuojaseloste', { webuser: webuser });
});

module.exports = router;
