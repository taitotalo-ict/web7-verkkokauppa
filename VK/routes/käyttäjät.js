var express = require('express');
var router = express.Router();
let { Käyttäjä } = require('../models');
const bcrypt = require('bcrypt'); // Password encryption
const { body, validationResult } = require('express-validator');


function login(käyttäjä, istunto) {
  istunto.user = {
      id: käyttäjä.id,
      käyttäjätunnus: käyttäjä.käyttäjätunnus,
      isAdmin: käyttäjä.isAdmin
  }
}

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/kirjaudu', (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;
  
  if (webuser) {
    res.redirect('/');
  } else {
    res.render('kirjaudu', { webuser: null });
  }
});

router.post('/kirjaudu', async (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;

  if (webuser) {
    res.redirect('/');
  } else {
    // TODO: Tarkista tietoa lomakkeesta
    käyttäjä = await Käyttäjä.findOne({ where: { käyttäjätunnus: req.body.käyttäjätunnus.toLowerCase() }})
    if (käyttäjä) {
      if (bcrypt.compare(req.body.salasana, käyttäjä.salasana)) {
        login(käyttäjä, req.session);
        res.redirect('/');
      }
    }
    res.render('kirjaudu', { webuser: null }); //Virheellinen tunnus tai salasana
  }
});


router.get('/rekisteroi', (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;

  if (webuser) {
    res.redirect('/');
  } else {
    lomakeTiedot = {
      käyttäjätunnus: '',
      etunimi: '',
      sukunimi: '',
      sähköposti: ''
    }
  
    res.render('rekisteröi', { webuser: null, lomakeErrors: {}, lomakeTiedot: lomakeTiedot });
  }
});

rekisteröinninTarkistus = [
  // Username is not empty, min length of 4, it's trimmed, lowercasered and escaped
  body('käyttäjätunnus')
      .trim().escape()
      .not().isEmpty().withMessage('Käyttäjätunnus on pakollinen')
      .isLength({min: 4}).withMessage('Käyttäjätunnuksen minimi pituus on 4 merkkiä')
      .isLength({max: 50}).withMessage('Käyttäjätunnuksen maksimi pituus on 50 merkkiä')
      .toLowerCase()
      // Username uses only letter, numbers and symbols: @, +, -, . and _
      .custom( value => {
        if (!value.match(/^[0-9a-zäöå@+\-_.]+$/)) {
            return Promise.reject('Kieletty merkki käyttäjätunnuksessa')
        } else { 
          return true
        }
      })
      // Username is not already taken
      .custom( async käyttäjätunnus => {
        käyttäjätunnus = await Käyttäjä.findOne({ where: {käyttäjätunnus: käyttäjätunnus}});
        if (käyttäjätunnus) {
            return Promise.reject('käyttäjätunnus on jo olemassa');
        } else { 
          return true
        }
      }),
  // First name is not empty and it's escaped
  body('etunimi')
      .trim().escape()
      .not().isEmpty().withMessage('Etunimi on pakollinen'),
  // Same to last name
  body('sukunimi')
      .trim().escape()
      .not().isEmpty().withMessage('Sukunimi on pakollinen'),
  // email must be an email and it's trimmed
  body('sähköposti')
      .trim().escape()
      .isEmail(),
  // password must be at least 8 chars long
  body('salasana1')
      .isLength({ min: 8 }).withMessage('Salasanan minimi pituus on 8 merkkiä.')
      .custom((value, { req }) => {
        if (value !== req.body.salasana2) {
            throw new Error('Salasanat eivät täsmä');
        } else {
            return true
        }})
];

router.post('/rekisteroi', rekisteröinninTarkistus, async (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;

  if (webuser) {
    res.redirect('/');
    return
  }

  const lomakeErrors = validationResult(req);
  errorMsgs = {}
  lomakeErrors.errors.forEach( item => { 
    if (!errorMsgs[item.param]) {
      errorMsgs[item.param]=[]
    };
    errorMsgs[item.param].push(item.msg) 
  });
  
  lomakeTiedot = {
    käyttäjätunnus: req.body.käyttäjätunnus ? req.body.käyttäjätunnus : '',
    etunimi: req.body.etunimi ? req.body.etunimi : '',
    sukunimi: req.body.sukunimi ? req.body.sukunimi : '',
    sähköposti: req.body.sähköposti ? req.body.sähköposti : ''
  }

  if (lomakeErrors.isEmpty()) {
    let isAdmin = await Käyttäjä.count() ? false : true;
  
    // TODO: Tarkista lomakkeen tiedot
    // käyttäjätunnus, etunimi, sukunimi, sähköposti, salasana1, salasana2
    
  
    // Prepare password encryption
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(req.body.salasana1, salt);
  
    // const hash = bcrypt.hashSync(req.body.salasana1, bcrypt.genSaltSync(10));
  
    let käyttäjä = Käyttäjä.create({
      käyttäjätunnus: req.body.käyttäjätunnus.toLowerCase(),
      etunimi: req.body.etunimi,
      sukunimi: req.body.sukunimi,
      sähköposti: req.body.sähköposti,
      salasana: hash,
      isAdmin: isAdmin
    });
  
    login(käyttäjä, req.session);
  
    res.redirect('/');
  } else {
    res.render('rekisteröi', { webuser: null, lomakeErrors: errorMsgs, lomakeTiedot: lomakeTiedot });
  }

});

router.get('/login', (req, res, next) => {
  res.redirect(req.baseUrl+'/kirjaudu');
});

router.get('/register', (req, res, next) => {
  res.redirect(req.baseUrl+'/rekisteroi');
});

router.get('/kirjauduulos', (req, res, next) => {
  let webuser = req.session.user ? req.session.user : null;

  if (webuser) {
      req.session.destroy();
  }
  res.redirect('/');
});

module.exports = router;
